<?php

namespace App\Entity;

use App\abService\ProjectBaseBundle\Entity\TimestampableTrait;
use App\Repository\TransactionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 * @ORM\Table(name="transactions")
 * @ORM\HasLifecycleCallbacks
 */
class Transaction
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $transactionOrder;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $websiteOrderId;

    /**
     * @ORM\Column(type="float")
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $currency = "EUR";

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $extraData = [];

    /**
     * @ORM\ManyToOne(targetEntity=Gateway::class, inversedBy="transactions")
     */
    private $gateway;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $gatewayExtraFields = [];

    /**
     * @ORM\ManyToOne(targetEntity=Website::class, inversedBy="transactions")
     */
    private $website;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $userData = [];

    /**
     * @ORM\Column(type="integer",
     *     options={
     *     "default" : 1,
     *     "comment":"1: created, 2:Pending, 3:Refused, 4:Accepted, 5:Cancelled, 6:Refunded, "})
     */
    private $status = 1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $returnUrl;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTransactionOrder(): ?string
    {
        return $this->transactionOrder;
    }

    public function setTransactionOrder(string $transactionOrder): self
    {
        $this->transactionOrder = $transactionOrder;

        return $this;
    }

    public function getWebsiteOrderId(): ?string
    {
        return $this->websiteOrderId;
    }

    public function setWebsiteOrderId(?string $websiteOrderId): self
    {
        $this->websiteOrderId = $websiteOrderId;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getExtraData(): ?array
    {
        return $this->extraData;
    }

    public function setExtraData(array $extraData): self
    {
        $this->extraData = $extraData;

        return $this;
    }

    public function getGateway(): ?Gateway
    {
        return $this->gateway;
    }

    public function setGateway(?Gateway $gateway): self
    {
        $this->gateway = $gateway;

        return $this;
    }

    public function getGatewayExtraFields(): ?array
    {
        return $this->gatewayExtraFields;
    }

    public function setGatewayExtraFields(?array $gatewayExtraFields): self
    {
        $this->gatewayExtraFields = $gatewayExtraFields;

        return $this;
    }

    public function getWebsite(): ?Website
    {
        return $this->website;
    }

    public function setWebsite(?Website $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getUserData(): ?array
    {
        return $this->userData;
    }

    public function setUserData(array $userData): self
    {
        $this->userData = $userData;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Generate random transaction order
     * @return $this
     */
    public function generateTransactionOrder(){

        $this->transactionOrder = $this->generateRandomString("10");

        return $this;
    }

    public function getReturnUrl(): ?string
    {
        return $this->returnUrl;
    }

    public function setReturnUrl(?string $returnUrl): self
    {
        $this->returnUrl = $returnUrl;

        return $this;
    }
}
