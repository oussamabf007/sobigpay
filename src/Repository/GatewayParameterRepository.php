<?php

namespace App\Repository;

use App\abService\ProjectBaseBundle\Repository\baseRepository;
use App\Entity\GatewayParameter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GatewayParameter|null find($id, $lockMode = null, $lockVersion = null)
 * @method GatewayParameter|null findOneBy(array $criteria, array $orderBy = null)
 * @method GatewayParameter[]    findAll()
 * @method GatewayParameter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GatewayParameterRepository extends baseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GatewayParameter::class);
    }

    // /**
    //  * @return GatewayParameter[] Returns an array of GatewayParameter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GatewayParameter
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
