<?php
/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 05/10/20
 * Time: 20:58
 */

namespace App\abService\CoreBundle\Controller;

use App\abService\ProjectBaseBundle\Controller\AbstractController;
use App\abService\CoreBundle\Form\UserFormType;
use App\abService\ProjectBaseBundle\Services\MailerSender;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/clients", name="clients_")
 */
class ClientsController extends AbstractController
{
    //user repository.
    private $userRepository;

    //EntityManager.
    private $entityManager;

    /**
     * ClientsController constructor.
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="clients_index")
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NoResultException
     */
    public function index(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            //Getrequested data.
            $data = $request->query->all();
            //Get data.
            $ajaxDataResults = $this->userRepository->getAjaxDataTableData($data);
            //Set the data as wanted.
            $ajaxDataResults['data'] = $this->formatData($ajaxDataResults['data']);
            $ajaxDataResults['recordsFiltered'] = count($ajaxDataResults['data']);
            //return data.
            return new JsonResponse($ajaxDataResults);
        }
        return $this->render('clients/index.html.twig', []);
    }

    /**
     * @Route("/edit/{token}", name="clients_edit")
     *
     * @param Request $request
     * @param $token
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editClient(Request $request, $token)
    {
        //Get the requested user by his token.
        $userToModify = $this->userRepository->findOneByToken($token);

        //If user not found throw error.
        if (is_null($userToModify))
            throw $this->createNotFoundException('The user does not exist');

        //Create user edit form.
        $form = $this->createForm(UserFormType::class, $userToModify);
        //handle request.
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                //Save data to database.
                $this->entityManager->persist($userToModify);
                $this->entityManager->flush();
                $this->addFlash('success', "User was updated successfully");
            } else {
                foreach ($form->getErrors() as $error) {
                    $this->addFlash('error', $error->getMessage());
                }
            }
        }

        return $this->render('clients/edit.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/new", name="add_client")
     * @param Request $request
     * @param MailerSender $mailerSender
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function addClient(Request $request, MailerSender $mailerSender)
    {
        $user = new User();

        //Create user edit form.
        $form = $this->createForm(UserFormType::class, $user);
        //handle request.
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                //Generate token to allow access.
                $user->resetTokenGenerator(40);
                //Save data to database.
                $this->entityManager->persist($user);
                $this->entityManager->flush();
                $this->addFlash('success', "User was added successfully");

                //trying to send mail to the new user.
                $mailerSenderResult = $mailerSender->sendMail(
                    [$user->getEmail()],
                    "New account created",
                    "clients/welcome_message.html.twig",
                    array("newUser" => $user));
                if ($mailerSenderResult)
                    $this->addFlash('success', "An email sent successfully to " . $user->getEmail());

            } else {
                foreach ($form->getErrors() as $error) {
                    $this->addFlash('error', $error->getMessage());
                }
            }
        }

        return $this->render('clients/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param $data
     *
     * Set the data to correspond with the datatable format.
     * @return array
     */
    private function formatData($data)
    {

        $result = [];
        $dataRow = [];

        foreach ($data as $user) {
            $dataRow['t_id'] = $user['t_id'];
            $dataRow['t_firstName'] = $user['t_firstName'];
            $dataRow['t_lastName'] = $user['t_lastName'];
            $dataRow['t_email'] = $user['t_email'];
            $dataRow['t_lastLogin'] = $user['t_lastLogin'];
            $dataRow['t_action'] = "<a href='" .
                $this->generateUrl('clients_clients_edit', ['token' => $user['t_token']]) .
                "' title='edit'><i class='fa fa-edit fa-2x'></i></a>";

            if ($user['t_isActivated']) {
                $dataRow['t_isActivated'] = "<span class='badge badge-success'>Activated</span>";
            } else {
                $dataRow['t_isActivated'] = "<span class='badge badge-secondary'>Deactivated</span>";
            }

            $result[] = $dataRow;
        }

        return $result;
    }
}