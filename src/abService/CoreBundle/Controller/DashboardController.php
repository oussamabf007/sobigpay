<?php

/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 05/10/20
 * Time: 20:58
 */

namespace App\abService\CoreBundle\Controller;

use App\abService\CoreBundle\Form\TransactionFilterType;
use App\abService\CoreBundle\Services\TransactionHelper;
use App\abService\ProjectBaseBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dashboard", name="dashboard_")
 */
class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="dashboard_index")
     * @param Request $request
     * @param TransactionHelper $transactionHelper
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, TransactionHelper $transactionHelper)
    {
        $transactionFilterForm = $this->createForm(TransactionFilterType::class);
        $gateways = $transactionHelper->getGateways($this->getCurrentUser());
        $randomColor = str_pad( dechex( mt_rand( 0, 255 ) ), 3, '0', STR_PAD_LEFT);

        return $this->render('dashboard/index.html.twig', [
            'transactionFilterForm' => $transactionFilterForm->createView(),
            'gateways' => $gateways
        ]);
    }

    /**
     * @Route("/get_data", name="dashboard_stats", methods={"POST"})
     */
    public function dashboardStats(Request $request, TransactionHelper $transactionHelper)
    {
        //get statistics
        $stats  = $transactionHelper->getStats($request, $this->getCurrentUser());
        return new JsonResponse($stats);
    }
}