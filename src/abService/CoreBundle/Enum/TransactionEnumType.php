<?php


/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 05/11/20
 * Time: 23:00
 */

namespace App\abService\CoreBundle\Enum;


class TransactionEnumType
{

    const ALL_STATUS = 0;
    const STATUS_CREATED = 1;
    const STATUS_PENDING = 2;
    const STATUS_REFUSED = 3;
    const STATUS_ACCEPTED = 4;
    const STATUS_CANCELLED = 5;
    const STATUS_REFUNED = 6;

    /**
     * Get all status.
     *
     * @return array
     */
    static function getAllStatus()
    {
        return array(
            TransactionEnumType::ALL_STATUS,
            TransactionEnumType::STATUS_CREATED,
            TransactionEnumType::STATUS_PENDING,
            TransactionEnumType::STATUS_REFUSED,
            TransactionEnumType::STATUS_ACCEPTED,
            TransactionEnumType::STATUS_CANCELLED,
            TransactionEnumType::STATUS_REFUNED,
        );
    }

    /**
     * Return all status name indexed by their value.
     * @return array
     */
    static function getAllStatusName()
    {
        return array(
            TransactionEnumType::ALL_STATUS => "All status",
            TransactionEnumType::STATUS_CREATED => "Created",
            TransactionEnumType::STATUS_PENDING => "Pending",
            TransactionEnumType::STATUS_REFUSED => "Refused",
            TransactionEnumType::STATUS_ACCEPTED => "Accepted",
            TransactionEnumType::STATUS_CANCELLED => "Cancelled",
            TransactionEnumType::STATUS_REFUNED => "Refuned",
        );
    }

    /**
     * Return status name by its status (int).
     *
     * @param $status
     * @return mixed
     */
    static function getStatusName($status)
    {
        return (static::getAllStatusName())[$status];
    }

}