<?php

namespace App\abService\CoreBundle\Services;

use App\Entity\Gateway;
use App\Entity\Transaction;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class TransactionHelper
{

    //Entity manager.
    public $em;

    //Authorisation checker.
    public $authChecker;

    //transaction repository.
    public $transactionRepository;

    //transaction repository.
    public $gatewayRepository;

    /**
     * transaction constructor.
     * @param EntityManagerInterface $entityManager
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(EntityManagerInterface $entityManager, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authChecker = $authorizationChecker;
        $this->em = $entityManager;
        $this->transactionRepository = $entityManager->getRepository(Transaction::class);
        $this->gatewayRepository = $entityManager->getRepository(Gateway::class);
    }

    /**
     * Get all current user transactions based on his role and assigned websites.
     * @param User $user
     * @return array Transaction
     */
    public function getTransactions(User $user)
    {
        if ($this->authChecker->isGranted("ROLE_ADMIN"))
            return $this->transactionRepository->findAll();
        else
            return $this->transactionRepository->findAllByUser($user);
    }

    /**
     * @param User $user
     *
     * @return array Gateway.
     */
    public function getGateways(User $user)
    {
        if ($this->authChecker->isGranted("ROLE_ADMIN"))
            return $this->gatewayRepository->findAll();
        else
            return $this->gatewayRepository->findAllUsedGatewayByUser($user);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return array
     */
    public function getStats(Request $request, User $user)
    {
        $result = [];
        //Get filter gateways and websites.
        $selectedGatewaysTokens = (json_decode($request->request->get("gateways")));
        $selectedWebsitesTokens = (json_decode($request->request->get("websites")));

        $transactionsGateways = $this->getTransactionsPerGateway($user, $selectedGatewaysTokens, $selectedWebsitesTokens);

        if (is_array($transactionsGateways))
            $result["transactions_gateways"] = $transactionsGateways;

        //Stats for transactions status per day.
        $transactionsStatus = $this->getTransactionsStatusPerDay($user, $selectedGatewaysTokens,$selectedWebsitesTokens);
        if (is_array($transactionsStatus))
            $result["transactions_status"] = $transactionsStatus;

        return $result;
    }

    /**
     * @param User $user
     * @param array $gatewaysToken
     * @param null $websitesToken
     * @return array|bool
     */
    public function getTransactionsPerGateway(User $user, $gatewaysToken = null, $websitesToken = null)
    {
        //Get transactions count by gateway.
        if ($this->authChecker->isGranted("ROLE_ADMIN"))
            $user = null;
        $transactionsPerGateway = $this->transactionRepository->getTransactionByGateways($user, $gatewaysToken, $websitesToken);

        //calculate transactions.
        $sum = 0;
        foreach ($transactionsPerGateway as $transactionPerGateway) {
            $sum += $transactionPerGateway["total_per_gateway"];
        }
        if ($sum > 0) {

            $labels = [];
            $colors = [];
            $data = [];
            foreach ($transactionsPerGateway as $transactionPerGateway) {
                if (is_null($transactionPerGateway["name"])) {
                    $transactionPerGateway["name"] = "Unspecified";
                    $transactionPerGateway["color"] = "6c757d";
                }
                $labels[] = $transactionPerGateway["name"];
                $colors[] = '#' . $transactionPerGateway["color"];
                $data[] = number_format(($transactionPerGateway["total_per_gateway"] * 100) / $sum, 3);
            }

            return array("labels" => $labels, "colors" => $colors, "data" => $data);
        }
        return false;

    }

    /**
     * @param User $user
     * @param null $gatewaysToken
     * @param null $websitesToken
     * @return array
     */
    public function getTransactionsStatusPerDay(User $user, $gatewaysToken = null, $websitesToken = null)
    {
        //Get transactions count by gateway.
        if ($this->authChecker->isGranted("ROLE_ADMIN"))
            $user = null;

        $transactionsStatusPerDay = $this->transactionRepository->getTransactionsStatusPerDay($user, $gatewaysToken, $websitesToken);

        $result = [];
        //Set the missing data (status with 0)
        foreach ($transactionsStatusPerDay as $transactionStatusPerDay) {
            $result[$transactionStatusPerDay['day']][$transactionStatusPerDay["status"]] = $transactionStatusPerDay["total_per_status"];
        }

        $data = [];
        foreach ($result as $date => $resultPerDay) {
            for ($i = 1; $i < 6; $i++) {
                if (!isset($resultPerDay[$i])) {
                    $data[$date][$i] = "0";
                } else {
                    $data[$date][$i] = $resultPerDay[$i];
                }
            }
        }

        if (count($result))
            return array("labels" => array_keys($result), "data" => $data);
        else
            return false;
    }
}


?>