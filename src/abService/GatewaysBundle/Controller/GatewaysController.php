<?php
/**
 * Created by PhpStorm.
 * User: abs
 * Date: 05/10/20
 * Time: 20:58
 */

namespace App\abService\GatewaysBundle\Controller;

use App\abService\GatewaysBundle\Form\GatewayFormType;
use App\abService\ProjectBaseBundle\Controller\AbstractController;
use App\Entity\Gateway;
use App\Repository\GatewayRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/gateways", name="gateways_")
 */
class GatewaysController extends AbstractController
{

    //user repository.
    private $gatewayRepository;

    //EntityManager.
    private $entityManager;

    /**
     * ClientsController constructor.
     * @param GatewayRepository $gatewayRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(GatewayRepository $gatewayRepository, EntityManagerInterface $entityManager)
    {
        $this->gatewayRepository = $gatewayRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="gateways_index")
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NoResultException
     */
    public function index(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            //Getrequested data.
            $data = $request->query->all();
            //Get data.
            $ajaxDataResults = $this->gatewayRepository->getAjaxDataTableData($data);
            //Set the data as wanted.
            $ajaxDataResults['data'] = $this->formatData($ajaxDataResults['data']);
            $ajaxDataResults['recordsFiltered'] = count($ajaxDataResults['data']);
            //return data.
            return new JsonResponse($ajaxDataResults);
        }

        return $this->render("gateway/index.html.twig", [
        ]);
    }

    /**
     * @Route("/new", name="add_gateway")
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NoResultException
     */
    public function addGateway(Request $request)
    {
        //Create new gateway.
        $gateway = new Gateway();

        //Create Form
        $form = $this->createForm(GatewayFormType::class, $gateway);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                //Save data to database.
                $this->entityManager->persist($gateway);
                $this->entityManager->flush();

                //Save gatewayParameters to database.
                foreach ($gateway->getGatewayParameters() as $gatewayParameter) {
                    $gatewayParameter->setGateway($gateway);
                    $this->entityManager->persist($gatewayParameter);
                }

                $this->entityManager->flush();
                $this->addFlash('success', "Gateway was added successfully");
            } else {
                foreach ($form->getErrors() as $error){
                    $this->addFlash('error', $error->getMessage());
                }
            }
        }

        return $this->render("gateway/new.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{token}", name="edit_gateway")
     * @param Request $request
     * @param $token
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editGateway(Request $request, $token)
    {
        //Create new gateway.
        $gateway = $this->gatewayRepository->findOneByToken($token);

        //If gateway not found throw error.
        if (is_null($gateway))
            throw $this->createNotFoundException('This gateway does not exist');

        //Clone Gateway to delete parameter.
        $gatewayClone = new Gateway();
        foreach ($gateway->getGatewayParameters()as $gatewayParameter){
            $gatewayClone->addGatewayParameter($gatewayParameter);
        }

        //Create Form
        $form = $this->createForm(GatewayFormType::class, $gateway);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                //Save gatewayParameters to database.
                foreach ($gateway->getGatewayParameters() as $gatewayParameter) {
                    $gatewayParameter->setGateway($gateway);
                    foreach ($gatewayParameter->getChildGatewayParameter() as $childGatewayParameter){
                        //Update parameter name.
                        $childGatewayParameter->setName($gatewayParameter->getName());
                        $childGatewayParameter->setIsRequired($gatewayParameter->getIsRequired());
                        $this->entityManager->persist($childGatewayParameter);
                    }
                    $this->entityManager->persist($gatewayParameter);
                }

                //Remove deleted GatewayParamaeters.
                foreach ($gatewayClone->getGatewayParameters() as $gatewayParameter){
                    if (!$gateway->getGatewayParameters()->contains($gatewayParameter)) {
                        //Delete childParameters before deeting baseGateway.
                        foreach ($gatewayParameter->getChildGatewayParameter() as $childGatewayParameter){
                            $this->entityManager->remove($childGatewayParameter);
                        }
                        $this->entityManager->remove($gatewayParameter);
                    }
                }

                //Save data to database.
                $this->entityManager->persist($gateway);
                //Flush data base.
                $this->entityManager->flush();

                $this->addFlash('success', "Gateway was updated successfully");
            } else {
                foreach ($form->getErrors() as $error){
                    $this->addFlash('error', $error->getMessage());
                }

            }

            $this->redirectToRoute("gateways_edit_gateway",array("token"=>$gateway->getToken()));
        }

        return $this->render("gateway/edit.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param $data
     *
     * Set the data to correspond with the datatable format.
     * @return array
     */
    private function formatData($data)
    {
        $result = [];
        $dataRow = [];

        foreach ($data as $gateway) {
            $dataRow['t_id'] = $gateway['t_id'];
            $dataRow['t_name'] = $gateway['t_name'];
            $dataRow['t_serviceId'] = $gateway['t_serviceId'];
            $dataRow['t_description'] = $gateway['t_description'];
            $dataRow['t_createdAt'] = $gateway['t_createdAt'];
            $dataRow['t_updatedAt'] = $gateway['t_updatedAt'];
            $dataRow['t_action'] = "<a href='" .
                $this->generateUrl('gateways_edit_gateway', ['token' => $gateway['t_token']]) .
                "' title='edit'><i class='fa fa-edit fa-2x'></i></a>";

            if ($gateway['t_isActivated']) {
                $dataRow['t_isActivated'] = "<span class='badge badge-success'>Activated</span>";
            } else {
                $dataRow['t_isActivated'] = "<span class='badge badge-secondary'>Deactivated</span>";
            }

            $result[] = $dataRow;
        }

        return $result;
    }
}