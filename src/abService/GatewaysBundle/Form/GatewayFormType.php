<?php


namespace App\abService\GatewaysBundle\Form;

use App\Entity\Gateway;
use App\Entity\GatewayParameter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GatewayFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'required' => true,
                'attr' => array('class' => "form-control"),

            ))
            ->add('color', TextType::class, array(
                'required' => true,
                'attr' => array('class' => "form-control"),

            ))
            ->add('serviceId', TextType::class, array(
                'required' => false,
                'attr' => array('class' => "form-control"),

            ))
            ->add('description', TextareaType::class, array(
                'required' => true,
                'attr' => array('class' => "form-control"),

            ))
            ->add("gatewayParameters", CollectionType::class, array(
                'entry_type' => GatewayParameterFormType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                'label'=> 'gateways parameters'
            ))
            ->add('extraFields', TextareaType::class, array(
                'attr' => array('class' => "form-control", "rows" => 8),
                'required' => false

            ))
            ->add('isActivated', CheckboxType::class, array(
                'attr' => array('class' => "form-control"),
                'required' => false

            ));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Gateway::class,
        ]);
    }
}