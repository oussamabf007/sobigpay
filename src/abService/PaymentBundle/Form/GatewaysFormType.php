<?php


namespace App\abService\PaymentBundle\Form;

use App\Entity\Gateway;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GatewaysFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $em = $options['data']['em'];
        $website = $options['data']['website'];
        $gateways = $em->getRepository(Gateway::class)->getActivatedGateways($website);

        $builder
            ->add('gateway', ChoiceType::class, [
                'label'=>"Select payment method",
                'expanded'=>true,
                'multiple'=>false,
                'choices' => $gateways,
                'choice_value' => 'token',
                'choice_label' => function(?Gateway $gateway) {
                    return $gateway ? strtoupper($gateway->getName()) : '-';
                }
            ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function getBlockPrefix()
    {
        return "transaction_method";
    }
}