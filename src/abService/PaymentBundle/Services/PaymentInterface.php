<?php
/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 05/11/20
 * Time: 23:00
 */

namespace App\abService\PaymentBundle\Services;

use App\Entity\Gateway;
use App\Entity\Transaction;
use App\Entity\Website;
USE App\abService\CoreBundle\Enum\TransactionEnumType;
use Symfony\Component\HttpFoundation\Request;

interface PaymentInterface{

    /**
     * Function called when user choose his gateway.
     * This function is called to process the payment by the choosen gateway.
     *
     * @param Transaction $transaction
     * @param array $configuration
     *          Contains:
     *              -gateway_param: wiche are the gateway parameters with there values configured in the gateway form.
     *              -transaction_data: wiche are the transaction extra data configured in the gateway form.
     *
     * @param string $returnUrl
     * @param string $callbackUrl
     * @return mixed
     */
    public function execute(Transaction $transaction, $configuration= [], $returnUrl ="", $callbackUrl="");

    /**
     * @param Transaction $transaction
     * @param array $configuration
     * @param Request $request
     * @return mixed
     */
    public function callback(Transaction $transaction, $configuration = [], Request $request);

}