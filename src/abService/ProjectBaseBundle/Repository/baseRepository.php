<?php

namespace App\abService\ProjectBaseBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr;
use LogicException;

class baseRepository extends ServiceEntityRepository
{
    /**
     * baseRepository constructor.
     * @param ManagerRegistry $registry
     * @param $entityClass
     */
    public function __construct(ManagerRegistry $registry, $entityClass = "")
    {
        parent::__construct($registry, $entityClass);
    }

    /**
     * @param $params
     * @return array
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getAjaxDataTableData($params)
    {
        //Get requested field.
        if (isset($params['columns'])) {
            $columns = 't.token as t_token,';
            $columns .= ' t.id as t_id,';

            foreach ($params['columns'] as $colum) {
                if ($colum['name'] != "" && false === strpos($columns, $colum['name'])) {
                    $columns .= $colum['name'] . ' AS ' . $colum['data'] . ',';
                }
            }
        } else {
            $columns = 't';
        }

        //Get requested hidden columns
        if (isset($params['hidden_columns'])) {
            foreach ($params['hidden_columns'] as $colum) {
                if ($colum['name'] != "" && false === strpos($columns, $colum['name'])) {
                    $columns .= $colum['name'] . ' AS ' . $colum['data'] . ',';
                }
            }
        }

        //Prepare requests.
        $qb = $this->createQueryBuilder('t')->select(rtrim($columns, ','));
        $total = $this->createQueryBuilder('t')->select('count(t.id)');
        $FilteredTotal = clone $total;

        //Join entities.
        if (isset($params['join'])) {
            foreach ($params['join'] as $join) {
                //Added by abs & haykelJulia & bilel to add left join to this datatable.
                if (isset($join['type']) && 'left' == $join['type']) {
                    $qb->leftJoin($join['join'], $join['alias'], Expr\Join::WITH, $join['condition']);
                    $FilteredTotal->leftJoin($join['join'], $join['alias'], Expr\Join::WITH, $join['condition']);
                } else {
                    $qb->innerJoin($join['join'], $join['alias'], Expr\Join::WITH, $join['condition']);
                    $FilteredTotal->innerJoin($join['join'], $join['alias'], Expr\Join::WITH, $join['condition']);
                }
            }
        }

        //Set the first row.
        if (isset($params['start']) and null != $params['start']) {
            $qb->setFirstResult((int)$params['start']);
        }

        //Set the last row
        if (isset($params['length']) and null != $params['length']) {
            $qb->setMaxResults((int)$params['length']);
        }

        //Order the data.
        if (isset($params['order'])) {
            foreach ($params['order'] as $order) {
                if ($params['columns'][$order['column']]['name'] != "" && $params['columns'][$order['column']]['orderable']) {
                    $qb->addOrderBy($params['columns'][$order['column']]['name'], $order['dir']);
                }
            }
        }

        //Set condition.
        if (isset($params['conditions'])) {
            if (is_array($params['conditions']) && count($params['conditions'])) {
                foreach ($params['conditions'] as $condition) {
                    $qb->andWhere($condition);
                }
            } elseif ($params['conditions'] != "") {
                $qb->andWhere(str_replace(",", " AND ", $params['conditions']));
            }
        }

        //Searched data.
        $search = [];
        if (isset($params['columns']) and isset($params['search']) and $params['search']['value'] != '') {
            foreach ($params['columns'] as $column) {
                //if this column is seachable and has a name.
                if ('true' == $column['searchable'] && $column['name'] != "") {
                    //If this search field has a specifique search configuration.
                    if (isset($params['search_options'][$column['data']])) {
                        //Get the search field special configuration.
                        $searchValue = $this->extraResearchFields($params['search']['value'], $params['search_options'][$column['data']]);
                        if ($searchValue != "") {
                            $search[] = $column['name'] . " like '" . $searchValue . "'";
                        }
                    } else {
                        $search[] = $column['name'] . " like '%" . $params['search']['value'] . "%'";
                    }
                }
            }

            //Search on hidden columns.
            if (isset($params['hidden_columns'])) {
                foreach ($params['hidden_columns'] as $column) {
                    //if this column is seachable and has a name.
                    if ('true' == $column['searchable'] && $column['name'] != "") {
                        //If this search field has a specifique search configuration.
                        if (isset($params['search_options'][$column['data']])) {
                            //Get the search field special configuration.
                            $searchValue = $this->extraResearchFields($params['search']['value'], $params['search_options'][$column['data']]);
                            if ($searchValue != "") {
                                $search[] = $column['name'] . " like '" . $searchValue . "'";
                            }
                        } else {
                            $search[] = $column['name'] . " like '%" . $params['search']['value'] . "%'";
                        }
                    }
                }
            }
        }

        if (0 != count($search)) {
            $qb->andWhere(new Expr\Orx($search));
            $FilteredTotal->andWhere(new Expr\Orx($search));
        }

        try {
            $recordsTotal = $total->getQuery()->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            $recordsTotal = 0;
        }

        try {
            $recordsFiltered = $FilteredTotal->getQuery()->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            $recordsFiltered = 0;
        }

        $output = [
            'request' => $params,
            'draw' => $params['draw'],
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => $qb->getQuery()->getScalarResult(),
        ];

        return $output;
    }

    /**
     * @param $needle
     * @param $search_options
     * @return mixed
     */
    private function extraResearchFields($needle, $search_options)
    {
        //check if the searched exist in search options.
        foreach ($search_options as $search_option) {
            if (strtolower($needle) == strtolower($search_option['replaced_by'])) {
                return $search_option['original value'];
            }
        }
        return "";
    }
}