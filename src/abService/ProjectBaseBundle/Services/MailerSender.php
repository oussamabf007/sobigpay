<?php

namespace App\abService\ProjectBaseBundle\Services;

use \Twig\Environment as twigEnvironment;

class MailerSender
{
    private $mailer;
    private $templating;

    /**
     * MailerSender constructor.
     * @param \Swift_Mailer $mailer
     * @param twigEnvironment $templating
     */
    public function __construct(\Swift_Mailer $mailer, twigEnvironment $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    /**
     * @param $recipients
     * @param $mailObject
     * @param $templateName
     * @param array $parameters
     * @return int
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendMail($recipients, $mailObject, $templateName, $parameters = [])
    {
        $message = (new \Swift_Message($mailObject))
            ->setFrom('ghozia.abdessalem@gmail.com')
            ->setTo($recipients)
            ->setCc('ghozia.abdessalem@gmail.com')
            ->setBody(
                $this->templating->render($templateName, $parameters),
                'text/html'
            );

        return $this->mailer->send($message);
    }
}