<?php

namespace App\abService\UserBundle\Controller;

use App\abService\ProjectBaseBundle\Controller\AbstractController;
use App\abService\UserBundle\Form\EditProfileFormType;
use App\abService\UserBundle\Form\UserChangePasswordFormType;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfileController extends AbstractController
{

    public const LAST_EMAIL = 'user_login_last_email';

    //user repository.
    private $userRepository;

    //EntityManager.
    private $entityManager;

    //UserPasswordEncoderInterface.
    private $passwordEncoder;

    /**
     * ProfileController constructor.
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/profile/edit", name="user_profile_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editProfile(Request $request)
    {

        //Get connected User.
        $currentConnectedUser = $this->get('security.token_storage')->getToken()->getUser();

        //Create edit profile Form.
        $form = $this->createForm(EditProfileFormType::class, $currentConnectedUser);
        //handle request.
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            //Get the password sended by the user.
            $oldPassword = $form->get('plaintPassword')->getData();
            $form->get('plaintPassword')->setData("");
            //If the old password is correct.
            if ($this->passwordEncoder->isPasswordValid($currentConnectedUser, $oldPassword)) {

                //Save data to database.
                $this->entityManager->persist($currentConnectedUser);
                $this->entityManager->flush();

                $this->addFlash('success', "Profile was updated successfully");
            } else {
                $this->addFlash('error', "Wrong password! please write your current password");
            }
        }

        return $this->render('profile/edit_profile.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/profile/change_password", name="user_profile_change_password", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changePassword(Request $request)
    {
        //Create changePassword Form
        $form = $this->createForm(UserChangePasswordFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //Get connected User.
            $currentConnectedUser = $this->get('security.token_storage')->getToken()->getUser();
            //Get requested data.
            $data = $form->getData();
            $oldPassword = $data['current_password'];
            $newPassword = $data['new_password'];
            //If the old password is correct.
            if ($this->passwordEncoder->isPasswordValid($currentConnectedUser, $oldPassword)) {
                //Change user password.
                $currentConnectedUser->setPassword($this->passwordEncoder->encodePassword($currentConnectedUser, $newPassword));
                //persist changes.
                $this->entityManager->persist($currentConnectedUser);
                $this->entityManager->flush();

                $this->addFlash('success', "Password has been changed successfully");
            } else {
                $this->addFlash('error', "Wrong password");
            }
        }

        return $this->render('profile/change_password.html.twig', [
            'form' => $form->createView()
        ]);
    }
}