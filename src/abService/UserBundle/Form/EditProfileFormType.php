<?php


namespace  App\abService\UserBundle\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditProfileFormType extends AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('firstName', TextType::class,array(
                'required' => true,
                'attr'=>array('class'=>"form-control"),

            ))
            ->add('lastName', TextType::class,array(
                'required' => true,
                'attr'=>array('class'=>"form-control"),

            ))
            ->add('email', EmailType::class,array(
                'required' => true,
                'attr'=>array('class'=>"form-control"),

            ))
            ->add('plaintPassword', PasswordType::class,array(
                'mapped'=>false,
                'required' => true,
                'attr'=>array('class'=>"form-control", 'placeholder'=>"please enter your password", "value"=>""),
                'label' => 'Password',
            ));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}