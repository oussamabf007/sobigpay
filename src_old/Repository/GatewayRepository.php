<?php

namespace App\Repository;

use App\Entity\Gateway;
use App\abService\ProjectBaseBundle\Repository\baseRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Gateway|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gateway|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gateway[]    findAll()
 * @method Gateway[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GatewayRepository extends baseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gateway::class);
    }

    /**
     * Get activated gateways based on this website
     *
     * @param $website
     * @return mixed
     */
    public function getActivatedGateways($website)
    {
        return
            $this->createQueryBuilder('g')
                ->leftJoin("g.websiteGatewayConfigurations", 'wgc')
                ->leftJoin("wgc.website", 'w')
                ->andWhere('w = :website')
                ->andWhere("wgc.isActivated = 1")
                ->setParameter('website', $website)
                ->getQuery()
                ->getResult();
    }

    /**
     * @param $user
     * @return mixed
     */
    public function findAllUsedGatewayByUser($user){
        //queryBuilder.
        $queryBuilder = $this->createQueryBuilder('g')
            ->leftJoin('g.transactions', 't')
            ->leftJoin('t.website', 'tw')
            ->leftJoin('tw.users', 'twu')
            ->where('twu = :user')
            ->setParameter('user',$user);

        return $queryBuilder->getQuery()->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Gateway
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
