<?php

/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 05/10/20
 * Time: 20:58
 */

namespace App\abService\CoreBundle\Controller;

use App\abService\CoreBundle\Form\TransactionFilterType;
use App\abService\CoreBundle\Services\TransactionHelper;
use App\abService\ProjectBaseBundle\Controller\AbstractController;
use App\Entity\Transaction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/transaction", name="transactions_")
 */
class TransactionsController extends AbstractController
{
    /**
     * @Route("/", name="transactions_index")
     * @param Request $request
     * @param TransactionHelper $transactionHelper
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $transactionRepository = $this->getDoctrine()->getRepository(Transaction::class);

        if ($request->isXmlHttpRequest()) {
            //Getrequested data.
            $data = $request->query->all();

            //Add control on user role to add user.id in condition
            //This control is made in controller for security reasons.
            if (!$this->isGranted("ROLE_ADMIN")) {
                $data['join'][] = array(
                    "join" => "website.users",
                    "alias" => 'user',
                    "condition" => "user.id = " . $this->getCurrentUser()->getId()
                );
            }

            //Get data.
            $ajaxDataResults = $transactionRepository->getAjaxDataTableData($data);

            //Set the data as wanted.
            $ajaxDataResults['data'] = $this->formatData($ajaxDataResults['data']);
            $ajaxDataResults['recordsFiltered'] = count($ajaxDataResults['data']);

            //return data.
            return new JsonResponse($ajaxDataResults);
        }

        $transactionFilterForm = $this->createForm(TransactionFilterType::class);

        //Create formFilter.
        return $this->render('transactions/index.html.twig', [
            'transactionFilterForm' => $transactionFilterForm->createView()
        ]);
    }

    /**
     * @Route("/detail/{token}", name="transactions_detail")
     * @param Request $request
     * @param $token
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detail(Request $request, $token)
    {
        $transaction = $this->getDoctrine()->getRepository(Transaction::class)->findOneByToken($token);

        if (is_null($transaction)) {
            throw $this->createNotFoundException('Transaction not found');
        } elseif (!$this->isGranted("ROLE_ADMIN") && !$this->getCurrentUser()->getWebsites()->contains($transaction->getWebsite())) {
            $this->addFlash('error', "You are not allowed!");
            return $this->redirectToRoute("transactions_transactions_index");
        }

        //@TODO: Find a perfect template to show all transaction details.
        dd("under construction");
        return $this->render('transactions/detail.html.twig', ['transaction' => $transaction]);
    }

    private function formatData($data)
    {
        $result = array();

        $transactionStatus = array(
            1 => "<span class='badge badge-primary'>Created</span>",
            2 => "<span class='badge badge-secondary'>Pending</span>",
            3 => "<span class='badge badge-danger'>Refused</span>",
            4 => "<span class='badge badge-success'>Accepted</span>",
            5 => "<span class='badge badge-warning'>Cancelled</span>",
            6 => "<span class='badge badge-info'>Refunded</span>",
        );

        //format data.
        foreach ($data as $transactionData) {
            $resultRow['t_transactionOrder'] = $transactionData['t_transactionOrder'];
            $resultRow['t_url'] = $transactionData['t_url'];
            $resultRow['t_amount'] = $transactionData['t_amount'] . " (" . $transactionData['t_currency'] . ")";
            $resultRow['t_gateway'] = is_null($transactionData['t_gateway']) ? "-" : $transactionData['t_gateway'];
            $resultRow['t_status'] = $transactionStatus[$transactionData['t_status']];
            $resultRow['t_createdAt'] = $transactionData['t_createdAt'];
            $resultRow['t_updatedAt'] = $transactionData['t_updatedAt'];
            $resultRow['t_action'] = "<a href='" .
                $this->generateUrl('transactions_transactions_detail', ['token' => $transactionData["t_token"]]) .
                "' title='edit'><i class='fa fa-eye fa-2x'></i></a>";

            $result[] = $resultRow;
        }

        return $result;
    }
}