<?php


namespace  App\abService\CoreBundle\Form;

use App\Entity\Website;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WebsiteFormType extends AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('name', TextType::class,array(
                'required' => true,
                'attr'=>array('class'=>"form-control"),

            ))
            ->add('url', TextType::class,array(
                'required' => true,
                'attr'=>array('class'=>"form-control"),

            ))
            ->add('ipAddress', TextType::class,array(
                'required' => false,
                'attr'=>array('class'=>"form-control"),

            ))
            ->add('callbackUrl', UrlType::class,array(
                'required' => true,
                'attr'=>array('class'=>"form-control"),

            ))
            ->add('isActivated', CheckboxType::class,array(
                'attr'=>array('class'=>"form-control"),
                'required'=>false

            ));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Website::class,
        ]);
    }
}