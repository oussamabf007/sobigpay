<?php

/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 05/11/20
 * Time: 23:00
 */

namespace App\abService\PaymentBundle\Services;

use App\Entity\Transaction;
use App\Repository\WebsiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;

interface EDahabServiceInterface extends PaymentInterface
{
}