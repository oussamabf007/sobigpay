<?php

/**
 * Created by PhpStorm.
 * User: Abdessalem Ghozia (abs)
 * e-mail: ghozia.abdessalem@gmail.com
 *
 * Date: 27/10/20
 * Time: 23:00
 */


namespace App\abService\PaymentBundle\Services;

use App\Repository\WebsiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

interface TransactionManagerInterface extends ServiceSubscriberInterface{

    /**
     * WE will display this error when wrong secret key is requested.
     */
    //Website not found error.
    const PARAMETERS_SECRET_KEY_IS_REQUIRED = "Parameter Required : Secret key";

    /**
     * WE will display this error when wrong secret key is requested.
     */
    //Website not found error.
    const WEBSITE_HAS_NO_GATEWAY_ERROR = "No gateway found for this website";

    /**
     * WE will display this error when wrong secret key is requested.
     */
    //Website not found error.
    const WEBSITE_NOTE_FOUND_ERROR = "Invalide data requested";

    /**
     * WE will display this error when wrong secret key is requested.
     */
    //Website not found error.
    const WEBSITE_ACTIVATED_ERROR = "This website is deactivated";

    /**
     * WE will display this error when wrong secret key is requested.
     */
    //Website not found error.
    const SECURITY_ERROR = "Security check invalid";

    /**
     * WE will display this error when wrong secret key is requested.
     */
    //Website not found error.
    const MISSING_PARAMETERS = "Missing parameter";

    /**
     * WE will display this error when wrong secret key is requested.
     */
    //Website not found error.
    const INVALID_TYPE = "__PARAMETER__ must be __TYPE__";

    //Hash algo.
    const HASH_ALGO = "sha256";

}