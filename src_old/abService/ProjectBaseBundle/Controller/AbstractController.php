<?php
/**
 * Created by PhpStorm.
 * User: abs
 * Date: 05/10/20
 * Time: 20:58
 */

namespace App\abService\ProjectBaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as baseAbstractController;

class AbstractController extends baseAbstractController
{

    /**
     * Get current connected User
     * @return mixed
     */
    public function getCurrentUser(){
        return $this->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * @param $form
     */
    public function formErrorsFlashMessage($form){
        foreach ($form->getErrors() as $error){
            $this->addFlash('error', $error->getMessage());
        }
    }

}