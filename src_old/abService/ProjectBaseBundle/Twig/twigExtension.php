<?php
namespace App\abService\ProjectBaseBundle\Twig;

use App\abService\ProjectBaseBundle\Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class twigExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('isActive', [$this, 'isActive']),
            new TwigFunction('json_decode', [$this, 'jsonDecode']),
        ];
    }

    /**
     * @param $str
     * @return mixed
     */
    public function jsonDecode($str) {
        return json_decode($str);
    }

    /**
     * @param $currentRoutesName
     * @param $selectedRouteName
     * @return string
     */
    public function isActive($currentRoutesName, $selectedRouteName)
    {
        if(in_array($selectedRouteName, $currentRoutesName))
            return "active";
        else
            return "";
    }
}