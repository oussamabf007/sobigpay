<?php

namespace App\abService\UserBundle\Controller;

use App\abService\ProjectBaseBundle\Controller\AbstractController;
use App\abService\ProjectBaseBundle\Services\MailerSender;
use App\abService\UserBundle\Form\UserChangePasswordFormType;
use App\abService\UserBundle\Form\UserResetPasswordFormType;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoginController extends AbstractController
{

    public const LAST_EMAIL = 'user_login_last_email';

    //user repository.
    private $userRepository;

    //EntityManager.
    private $entityManager;

    //passwordEncoder.
    private $passwordEncoder;

    /**
     * LoginController constructor.
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/login", name="user_login", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        return $this->render('login/index.html.twig', []);
    }

    /**
     * generate token to resetpassword for the user.
     * @Route("/password_reset", name="user_password_reset", methods={"GET","POST"})
     *
     * @param Request $request
     * @param MailerSender $mailerSender
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function PasswordReset(Request $request, MailerSender $mailerSender)
    {
        //If user post his mail.
        if ($request->isMethod("POST") && $request->request->get("email")) {
            $email = $request->request->get("email");
            //Get user by email address.
            $user = $this->userRepository->findOneByEmail($email);
            if (!$user) {
                $this->addFlash("danger", "User not found!");
            } else {

                //Generate reset token.
                $user->resetTokenGenerator(45);

                //Save changes.
                $this->entityManager->persist($user);
                $this->entityManager->flush($user);

                //trying to send mail to the user.
                $mailerSenderResult = $mailerSender->sendMail(
                    [$user->getEmail()],
                    "Password reset request",
                    "login/reset_password_request.html.twig",
                    array("newUser" => $user));

                //add flash success message.
                if ($mailerSenderResult)
                    $this->addFlash("success", "A new password request has been sent to this address.");

                //set the reqesuted email address in the login form.
                $request->getSession()->set(LoginController::LAST_EMAIL, $email);

                //Redirect to login page.
                return $this->redirectToRoute("user_login");
            }

        }

        return $this->render('login/password_reset.html.twig', []);
    }

    /**
     * Get token that allows user to reset his password.
     *
     * @Route("/new_password/{resetToken}", name="user_profile_new_password", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newPassword(Request $request, $resetToken)
    {
        //Create reset Password Form
        $form = $this->createForm(UserResetPasswordFormType::class);

        $user = $this->userRepository->findOneByResetToken($resetToken);

        //If user not found throw error.
        if (is_null($user)) {
            return $this->redirectToRoute('user_login');
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //Get requested data.
            $data = $form->getData();

            //Get new user password.
            $newPassword = $data['new_password'];

            //Change user password.
            $user->setPassword($this->passwordEncoder->encodePassword($user, $newPassword));
            $user->setResetToken(null);
            //persist changes.
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            //Add flash message.
            $this->addFlash('success', "Password has been changed successfully");

            //set the reqesuted email address in the login form.
            $request->getSession()->set(LoginController::LAST_EMAIL, $user->getEmail());

            //Redirect user to login page.
            return $this->redirectToRoute('user_login');
        }

        return $this->render('login/new_password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/logout", name="user_logout", methods="GET")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout(Request $request)
    {
        die("test");
        return $this->redirectToRoute("user_login");

    }

}