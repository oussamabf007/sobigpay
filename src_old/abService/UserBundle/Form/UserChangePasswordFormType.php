<?php


namespace  App\abService\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserChangePasswordFormType extends AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('current_password', PasswordType::class,array(
                'required' => true,
                'attr'=>array('class'=>"form-control"),

            ))
            ->add('new_password', RepeatedType::class,array(
                'type' => PasswordType::class,
                'invalid_message' => 'the password field must be the same',
                'required' => true,
                'options' => array(
                    'attr'=>array('class'=>"form-control")
                ),
                'first_options'=>array("label"=>"New password", "label_attr"=>array("class"=>"test class")),
                'second_options'=>array("label"=>"Confirme new password", "label_attr"=>array("class"=>"test class"))
            ));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }
}