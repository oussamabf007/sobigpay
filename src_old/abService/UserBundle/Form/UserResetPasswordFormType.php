<?php


namespace  App\abService\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserResetPasswordFormType extends AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('new_password', RepeatedType::class,array(
                'type' => PasswordType::class,
                'invalid_message' => 'the password field must be the same',
                'required' => true,
                'options' => array(
                    'attr'=>array('class'=>"form-control")
                ),
                'first_options'=>array(
                    "label"=>"New password",
                    "label_attr"=>array("class"=>"test class"),
                    'attr'=>array('class'=>"form-control", "placeholder"=>"New password")),
                'second_options'=>array(
                    "label"=>"Confirm password",
                    "label_attr"=>array("class"=>"test class"),
                    'attr'=>array('class'=>"form-control", "placeholder"=>"Confirm password"))
            ));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }
}