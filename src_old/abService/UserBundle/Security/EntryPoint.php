<?php

namespace App\abService\UserBundle\Security;

use App\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;

class EntryPoint implements  AuthenticationEntryPointInterface{


    //TokenStorage Interface.
    private $token;

    private $urlGenerator;
    /**
     * EntryPoint constructor.
     * @param TokenStorageInterface $token
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(TokenStorageInterface $token, UrlGeneratorInterface $urlGenerator)
    {
        $this->token  = $token;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * Returns a response that directs the user to authenticate.
     *
     * This is called when an anonymous request accesses a resource that
     * requires authentication. The job of this method is to return some
     * response that "helps" the user start into the authentication process.
     *
     * Examples:
     *
     * - For a form login, you might redirect to the login page
     *
     *     return new RedirectResponse('/login');
     *
     * - For an API token authentication system, you return a 401 response
     *
     *     return new Response('Auth header required', 401);
     *
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return RedirectResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        if($this->token->getToken()){
            $routeName = 'dashboard_dashboard_index';
        }else{
            $routeName = "user_login";
        }

        return new RedirectResponse($this->urlGenerator->generate($routeName));
    }
}